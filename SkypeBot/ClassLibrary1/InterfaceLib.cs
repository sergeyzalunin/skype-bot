﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public interface IActionHandler
    {

        List<string> CallCommandList { get; }

        string CommandDescription { get; }

        void HandleMessage(string args, object clientData, Action<string> sendMessageFunc);
    }

    public interface IActionHandlerRegister
    {
        List<IActionHandler> GetHandlers();
    }

    public interface ISkypeData
    {
        string FromName { get; set; }
    }
}
