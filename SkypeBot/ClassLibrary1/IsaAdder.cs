﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace SkypeBot
{
    class IsaAdder : IActionHandler
    {
        private Random r = new Random();
        private List<string> answers = new List<string>()
        {
            "Вот сам и скажи",           
            "Ищи дурака",
            "Зачем?",
            "5$",
            "Нет, спасибо",
        };

        public List<string> CallCommandList
        {
            get { return new List<string>() { "скажи", "say" }; }
        }
        public string CommandDescription { get { return @"Говорит что прикажете"; } }
        public void HandleMessage(string args, object clientData, Action<string> sendMessageFunc)
        {
            if (args.StartsWith("/"))
            {
                sendMessageFunc(answers[r.Next(0, answers.Count - 1)]);
            }
            else
            {
                sendMessageFunc(args);
            }
        }
    }
}
